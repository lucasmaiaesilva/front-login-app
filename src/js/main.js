const $items = document.querySelector('.content__description')

function hide () {
  Array.prototype.forEach.call($items.children, (item) => {
    item.style.display = 'none'
  })
}

hide()
showBox('item1')

const $itemsMenu = document.querySelectorAll('.header__bottom-block__menu__item')
Array.prototype.forEach.call($itemsMenu, (item) => {
  item.addEventListener('click', handleClickMenu, false)
  item.style.cursor = 'pointer'
  item.classList.remove('active')
})

function handleClickMenu () {
  Array.prototype.forEach.call($itemsMenu, (item) => {
    item.classList.remove('active')
  })
  showBox(this.getAttribute('data-target'))
}

function showBox (id) {
  const element = document.querySelector('#' + id)
  const elementMenu = document.querySelector('[data-target=' + id + ']')
  hide()
  element.style.display = 'block'
  elementMenu.classList.add('active')
}
