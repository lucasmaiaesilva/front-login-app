module.exports = {
  entry: './src/js/main.js',
  output: {
    path: `${__dirname}/static`,
    filename: './js/bundle.js'
  },
  module: {
    loaders: [
      {
        test: /src\/\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          'presets': ['es2015', 'stage-0']
        }
      }
    ]
  }
}
