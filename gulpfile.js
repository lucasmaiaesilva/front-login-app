const gulp = require('gulp')
const sass = require('gulp-sass')
const minifyCSS = require('gulp-csso')
const browserSync = require('browser-sync')
const connect = require('gulp-connect')
const webpack = require('gulp-webpack')

const webpackConfig = require('./webpack.config.js')

gulp.task('sass', () => {
  return gulp.src('./src/sass/main.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('static/css'))
})

gulp.task('js', () => {
  return gulp.src('./src/js/**/*.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('static/'))
})

gulp.task('html', () => {
  return gulp.src('./src/**/*.html')
    .pipe(gulp.dest('static/'))
})

gulp.task('fonts', () => {
  return gulp.src('./src/fonts/**/*')
    .pipe(gulp.dest('static/fonts'))
})

gulp.task('images', () => {
  return gulp.src('./src/images/**/*')
    .pipe(gulp.dest('static/images'))
})

gulp.task('server', () => {
  const files = [
    './src/**/*.html',
    './src/sass/**/*.scss',
    './src/js/**/*.js'
  ]
  browserSync.init(files, {
    server: {
      baseDir: 'static/'
    }
  })
})

gulp.task('watch', () => {
  gulp.watch('./src/sass/**/*.scss', ['sass'])
  gulp.watch('./src/**/*.js', ['js'])
  gulp.watch('./src/**/*.html', ['html'])
})

gulp.task('default', ['html', 'fonts', 'images', 'sass', 'js', 'watch', 'server'])
